#!/usr/bin/python -tt
# Author: Anna Dowling 20/10/2014
#Script to check the status of Nginx, to run it and to stop it.
import commands
import sys
import os

def check_webserver():
  cmd = 'ps -Al | grep nginx | grep -v grep' #Checks the status of nginx
  (status, output) = commands.getstatusoutput(cmd) #Gets the console output of that status.

  if status:
    sys.stderr.write(output)
    print 'Not Running'
    cmd = 'sudo service nginx start' #Starts nginx if status is "Not Running".
    (status, output) = commands.getstatusoutput(cmd)
    if status:
      sys.stderr.write(output)
      print 'Not Running'
    else:
      print 'Running'
  else:
    print 'Running'


def main():
  check_webserver()

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  main()
