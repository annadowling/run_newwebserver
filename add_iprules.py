#!/usr/bin/python -tt

import iptc
import commands
import sys
#Script uses the iptables library -iptc to add rules to the ip-tables.

def menu():#Menu to select a rule
        print "Welcome to the IP Table Rules Menu!"
        ans = True
        while ans:
            print("""
            1. Default Rule Options
            2. HTTP
            3. HTTPS
            4. SSH
            5. EXIT
            """)
            ans = raw_input("Please choose an option number:")
            if ans == "1":
                defaultRule()
                
            elif ans == "2":
                allowHTTP()
                
            elif ans == "3":
                allowHTTPS()
                
            elif ans == "4":
                allowSSH()
                
            elif ans == "5":
                sys.exit(0)#Exit script


def dropAllInbound():    #drops all inbound traffic
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'INPUT')    #adds this chain to the iptable -Input
    rule = iptc.Rule()    #calls iptc library rule method
    rule.in_interface = 'eth+'    #selects the rule from ip interface, eth+ - all ethernet devices
    rule.target = iptc.Target(rule, 'DROP')    #Target specifies what to with a packet - DROP.
    chain.insert_rule(rule)    #inserts the new rule into the iptable chain

def allowLoopback():    #Allows loopback access on servers.
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'INPUT')
    rule = iptc.Rule()
    rule.in_interface = 'lo'
    rule.target = iptc.Target(rule, 'ACCEPT')    #Target specifies what to with a packet - ACCEPT.
    chain.insert_rule(rule)

def allowEstablishedInbound():
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'INPUT')
    rule = iptc.Rule()
    match = rule.create_match('state')     #Match -extension/ attribute creation
    match.state = 'RELATED,ESTABLISHED'    #RELATED- Packet is starting a new connection 
    #ESTABLISHED -packet is associated with a connection which has seen packets in both directions
    rule.target = iptc.Target(rule, 'ACCEPT')
    chain.insert_rule(rule)

def allowHTTP():
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'INPUT')
    rule = iptc.Rule()
    rule.in_interface = 'eth+'
    rule.protocol = 'tcp' #IP protocol for HTTP
    match = rule.create_match('tcp')
    match.dport = '80'#Listening for HTTP on port 80
    rule.target = iptc.Target(rule, 'ACCEPT')
    chain.insert_rule(rule)

def allowHTTPS():
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'INPUT')
    rule = iptc.Rule()
    rule.in_interface = 'eth+'
    rule.protocol = 'tcp' #IP protocol for HTTPS
    match = rule.create_match('tcp')
    match.dport = '443'    #Listening for HTTPS on port 443
    rule.target = iptc.Target(rule, 'ACCEPT')
    chain.insert_rule(rule)

def allowSSH():
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'INPUT')
    rule = iptc.Rule()
    rule.in_interface = 'eth+'
    rule.protocol = 'tcp'
    match = rule.create_match('tcp')
    match.dport = '22'    #Listening for SSH on port 22
    rule.target = iptc.Target(rule, 'ACCEPT')
    chain.insert_rule(rule)

def allowEstablishedOutbound():
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'OUTPUT')
    rule = iptc.Rule()
    match = rule.create_match('state')    #Match -extension/ attribute creation
    match.state = 'RELATED,ESTABLISHED'    #RELATED- Packet is starting a new connection 
    #ESTABLISHED -packet is associated with a connection which has seen packets in both directions
    rule.target = iptc.Target(rule, 'ACCEPT')
    chain.insert_rule(rule)

def dropAllOutbound():    #Drops all outbound traffic
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'OUTPUT')
    rule = iptc.Rule()
    rule.in_interface = 'eth+'
    rule.target = iptc.Target(rule, 'DROP')    #Drop this rule
    chain.insert_rule(rule)    #Inserts drop rule into the chain

def defaultRule():    #Default ip rule functions called here.
    dropAllOutbound()
    dropAllInbound()
    allowLoopback()
    allowEstablishedInbound()
    allowEstablishedOutbound()







def main():
    menu()

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  main()
