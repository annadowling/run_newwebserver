#!/usr/bin/python -tt
# Author: Anna Dowling 20/10/2014
# Python Script to create and add functionality to aws instance remotely.
import boto.ec2
import time
import sys
import commands
import iptc

my_key = 'annadowling_devops'
my_tag = 'TX_AnnaDowling'
instanceList = []   #Empty list to which the instance created is added.

class MenuClass:
    def __init__(self):
        print "Welcome to the aws menu!"
        ans = True
        while ans:
            print("""
            1. Run an Instance
            2. Install Nginx
            3. Check Nginx is Running
            4. Stop AWS Instance
            5. Terminate AWS Instance
            6. Console Output
            7. Set IP-table rules
            8. Exit
            """)
            ans = raw_input("Please choose an option number:")
            if ans == "1":
                Run_Instances()

            elif ans == "2":
                Install_Nginx()    

            elif ans == "3":
                Check_Running()

            elif ans == "4":
                Stop_Instance()

            elif ans == "5":
                Terminate_Instance()

            elif ans == "6":
                Console_Output()

            elif ans == "7":
                Install_Iptables()
                
            elif ans == "8":
                sys.exit(0)

class Run_Instances():
    def __init__(self):
        conn = boto.ec2.connect_to_region("eu-west-1")
        print 'Opening connection... '

        reservation = conn.run_instances('ami-892fe1fe',key_name=my_key,instance_type= 't2.micro',security_groups=['witsshrdp'])
        instance = reservation.instances[0] # Reservation object created on the instance.
        instanceList.insert(0, instance)  #Inserts the created instance into the list.
        instance.add_tag('Name', my_tag)  

        print 'Trying to create instance... '
        while instance.state != 'running':
            time.sleep(5) # Time pause before attempting to SSH in, created through the while loop.
            instance.update()
            print 'Still waiting... '
  
        print 'Instance ' + instance.id + ' now running...'
        print 'The public DNS name is ' + instance.public_dns_name

        print "Waiting 30 seconds before attempting to SSH in"
        time.sleep(30)

        cmd1 = "ssh -o StrictHostKeyChecking=no -i " + my_key + ".pem ec2-user@" + instance.public_dns_name + " 'pwd'"
        print cmd1
        (status, output) = commands.getstatusoutput(cmd1)
        while (status):
            print "Retrying"
            time.sleep(30) #Retries SSH in loops of 30 seconds.
            (status, output) = commands.getstatusoutput(cmd1)
        Command_Checker(status, output)


class Install_Nginx():
    def __init__(self):
        cmd2 = "scp -i " + my_key + ".pem check_webserver.py ec2-user@" + instanceList[0].public_dns_name + ":. " 
        print cmd2  #Secure copies check_webserver.py script to the instance.
        (status, output) = commands.getstatusoutput(cmd2)
        Command_Checker(status, output)
        cmd3 = "ssh -t -o StrictHostKeyChecking=no -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo yum -y install nginx'" #Installs Nginx
        print cmd3
        (status, output) = commands.getstatusoutput(cmd3)
        Command_Checker(status, output)



class Check_Running(): #Calls check_webserver.py script to check if Nginx is running.
    def __init__(self):
        cmd4 = "ssh -t -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo python check_webserver.py'"  
        print cmd4
        (status, output) = commands.getstatusoutput(cmd4)
        Command_Checker(status, output)


class Command_Checker():
    def __init__(self, status, output):
        if status:    ## Error case, print the command's output to stderr and exit
            sys.stderr.write(output)
            print "Failed"
            sys.exit(1)
        else:
            print output  ## Otherwise print the command's output
            print "Success"


class Stop_Instance():
    def __init__(self): 
        ans = raw_input("Would you like to stop your aws instance? y/n")
        if ans == "y":
            instanceList[0].stop() #Selects the instance from the list to stop.
            print "Your instance has been stopped."
        else:
            print "Still running!"


class Terminate_Instance(): #Deletes the instance created.
    def __init__(self):
        ans = raw_input("Would you like to terminate your aws instance? y/n")
        if ans == "y":
            instanceList[0].terminate() #Selects the instance from the list to delete.
            print "Your instance has been terminated."
        else:
            print "Your instance has not been terminated, please do so when usage is complete"


class Console_Output(): #Returns console output for the list instance.
    def __init__(self):
        output = instanceList[0].get_console_output()
        print output


class Install_Iptables():
    def __init__(self):
#Updates the instance.
        cmd6 = "ssh -t -o StrictHostKeyChecking=no -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo yum -y update'"
        print cmd6
        (status, output) = commands.getstatusoutput(cmd6)
        Command_Checker(status, output)

#Installs gcc
        cmd7 = "ssh -t -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo yum -y install gcc'"
        print cmd7
        (status, output) = commands.getstatusoutput(cmd7)
        Command_Checker(status, output)

#Installs python-devel
        cmd8 = "ssh -t -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo yum -y install python-devel'"
        print cmd8
        (status, output) = commands.getstatusoutput(cmd8)
        Command_Checker(status, output)

#Installs python-pip
        cmd9 = "ssh -t -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo yum -y install python-pip'"
        print cmd9
        (status, output) = commands.getstatusoutput(cmd9)
        Command_Checker(status, output)

#Installs python ip-tables
        cmd10 = "ssh -t -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo pip install python-iptables'"
        print cmd10
        (status, output) = commands.getstatusoutput(cmd10)
        Command_Checker(status, output)

# Upgrades python-iptables
        cmd11 = "ssh -t -i " + my_key + ".pem ec2-user@" + instanceList[0].public_dns_name + " 'sudo pip install --upgrade python-iptables'"
        print cmd11
        (status, output) = commands.getstatusoutput(cmd11)
        Command_Checker(status, output)



# Define a main() function.
def main():
    MenuClass()
    
# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  main()

